var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise 
const bacaBuku = function(time, index) {
    if (index <= books.length - 1) {
        readBooksPromise(time, books[index]).then(function(time){
            return bacaBuku(time, index + 1);
        }).catch(function(time){
            console.log('waktu yang dibutuhkan ' + Math.abs(time));
        })
    }
}

bacaBuku(10000, 0)
