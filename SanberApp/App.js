import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

// Tugas
import YoutubeUI from './Tugas/Tugas12/App';
import VideoItem from './Tugas/Tugas12/components/videoItem';
import LoginScreen from './Tugas/Tugas13/LoginScreen';
import AboutScreen from './Tugas/Tugas13/AboutScreen';
import RegisterScreen from './Tugas/Tugas13/RegisterScreen';
import SkillScreen from './Tugas/Tugas14/SkillScreen';
import Note from './Tugas/Tugas14/App';
import ReactNavTutor from './Tugas/Tugas15/index';
import AppSaya from './Tugas/TugasNavigation/index';
import Quiz3 from './Quiz3/index';


export default function App() {
  return (
    // Quiz 3
    <Quiz3 />

    // Tugas Navigation
    // <AppSaya />

    // Tugas 15
  //  <ReactNavTutor />

    // Tugas 14
    // <Note />
    // <SkillScreen />

    // Tugas 13
    // <RegisterScreen />
    // <AboutScreen />
    // <LoginScreen />

    // Tugas 12
    // <YoutubeUI />

    // <View style={styles.container}>
    //   <Text style={styles.h1}>Hello World!</Text>
    //   <StatusBar style="auto" />
    // </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#0b0c10',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h1: {
    fontSize: 50,
    color: '#66fcf1',
    alignItems: 'center',
    justifyContent: 'center',
  },
  p: {
    fontSize: 18,
    color: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  }
});
