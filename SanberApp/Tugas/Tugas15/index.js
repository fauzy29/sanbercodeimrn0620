import React from "react";
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { AuthContext } from "./context";
import { SignIn, CreateAccount, Search, Home, Details, Search2, Profile, Splash } from './Screen';

import { StyleSheet, Text, View } from "react-native";

const AuthStack = createStackNavigator();
const AuthStackScreen = () => {
  return (
  <AuthStack.Navigator>
  <AuthStack.Screen name='SignIn' component={SignIn} options={{title: 'Sign In'}}/>
  <AuthStack.Screen name='CreateAccount' component={CreateAccount} options={{title: 'Create Account'}}/>
  </AuthStack.Navigator>
  ) 
}

const Tabs = createBottomTabNavigator();
// menu bottom dan content nya
const TabsScreen = () => {
  return (
    <Tabs.Navigator>
    <Tabs.Screen name='Home' component={HomeStackScreen}/>
    <Tabs.Screen name='Search' component={SearchStackScreen} />
  </Tabs.Navigator>
  )
}
const HomeStack = createStackNavigator();
const SearchStack = createStackNavigator();

const HomeStackScreen = () => {
  return(
  <HomeStack.Navigator>
    <HomeStack.Screen name='Home' component={Home} />
    <HomeStack.Screen name='Details' component={Details} options={({route}) => ({
      title: route.params.name
    })}
    />
  </HomeStack.Navigator>
  )
}

const SearchStackScreen = () => {
  return(
  <SearchStack.Navigator>
    <HomeStack.Screen name='Search' component={Search} />
    <HomeStack.Screen name='Search2' component={Search2} />
  </SearchStack.Navigator>
  )
}

const ProfileStack = createStackNavigator();
const ProfileStackScreen = () => {
  return (
    <ProfileStack.Navigator>
    <ProfileStack.Screen name='Profile' component={Profile} />
  </ProfileStack.Navigator>
  )
}



const Drawer = createDrawerNavigator();
const DrawerScreen = () => {
  return (
    <Drawer.Navigator initialRouteName="Profile">
      <Drawer.Screen name='Home' component={TabsScreen} />
      <Drawer.Screen name='Profile' component={ProfileStackScreen} />
    </Drawer.Navigator>
  )
}

// controller
const RootStack = createStackNavigator();
const RootStackScreen = ({userToken}) => {
  return (
  <RootStack.Navigator headerMode='none'>
  {userToken ? (
    <RootStack.Screen name='App' component={DrawerScreen} option={{animationEnabled: false}}/>
  ) : (
    <RootStack.Screen name='Auth' component={AuthStackScreen}/>
  )}
  </RootStack.Navigator>
  )
}

export default () => {
  const [isLoading, setIsLoading] = React.useState(true);
  const [userToken, setUserToken]= React.useState(null);

  const authContext = React.useMemo(() => {
    return {
      signIn: () => {
        setIsLoading(false);
        setUserToken('asdf');
      },
      signUp: () => {
        setIsLoading(false);
        setUserToken('asdf');
      },
      signOut: () => {
        setIsLoading(false);
        setUserToken();
      }
    }
  }, [])

  React.useEffect(() => {
    setTimeout(() => {
      setIsLoading(false);
    }, 1000)
  }, []);

  if (isLoading) {
    return <Splash />;
  }

  return (
    <AuthContext.Provider value={authContext}>
    <NavigationContainer>
      <RootStackScreen userToken={userToken}/>
  </NavigationContainer>
  </AuthContext.Provider>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center"
  }
});
