import React from 'react';
import { StyleSheet, 
    Text, 
    View, 
    Image, 
    TouchableOpacity, 
    TextInput, 
    TouchableWithoutFeedback, 
    Keyboard,
    Button } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Icon2 from 'react-native-vector-icons/Ionicons';
import { Col, Row, Grid } from "react-native-easy-grid";
import Gitlab from './Gitlab';
import Github from './Github';


export default class AboutScreen extends React.Component {
    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.title}>Tentang Saya</Text>
                <Icon style={styles.user} name='account-circle' size={250} color='#EFEFEF'/>
                <Text style={styles.name}>Tubagus Gusti Fauzy</Text>
                <Text style={styles.desc}>Student</Text>
                <View style={styles.portofolio}>
                        <Text style={styles.portofTitle}>Portofolio</Text>
                <View style={styles.border}></View>
                    <View style={styles.git}>
                        <Gitlab style={styles.gitlab}/>
                        <Icon2 name='logo-github' size={50}/>
                    </View>
                    <View style={styles.accgit}>
                        <Text style={styles.akungitlab}>@fauzy29</Text>
                        <Text style={styles.akungithub}>@evans292</Text>
                    </View>
                </View>
                <View style={styles.contact}>
                <Text style={styles.portofTitle}>Hubungi Saya</Text>
                <View style={styles.border}></View>
                <Grid>
                    <Col style={styles.col1}>
                    <Icon2 name='logo-facebook' size={50} color='#3EC6FF'/>
                    <Icon2 name='logo-instagram' size={50} color='#3EC6FF'/>
                    <Icon2 name='logo-twitter' size={50} color='#3EC6FF'/>
                    </Col>
                    <Col style={styles.col2}>
                    <Text style={styles.fb}>Fauzy</Text>
                    <Text style={styles.ig}>@tubagusgf</Text>
                    <Text style={styles.tw}>@fravens26</Text>
                    </Col>
                </Grid>
                </View>
            </View> 
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    title: {
        textAlign: 'center',
        fontSize: 40,
        marginTop: 40,
        fontWeight: 'bold',
        color: '#003366'
    },
    name: {
        textAlign: 'center',
        fontSize: 32,
        marginTop: 5,
        fontWeight: 'bold',
        color: '#003366'
    },
    desc: {
        textAlign: 'center',
        fontSize: 24,
        marginTop: 5,
        fontWeight: 'bold',
        color: '#3EC6FF',
        marginBottom: 15
    },
    user: {
        marginLeft: 95,
    },
    portofolio: {
        width: 410,
        height: 120,
        backgroundColor: '#EFEFEF',
        marginLeft: 7,
        borderRadius: 8,
    },
    contact: {
        marginTop: 5,
        width: 410,
        height: 240,
        backgroundColor: '#EFEFEF',
        marginLeft: 7,
        borderRadius: 8,
    },
    portofTitle: {
        marginBottom: 5,
        marginLeft: 5,
        marginTop: 2,
        fontSize: 18,
        color: '#003366',
        fontWeight: 'bold'
    },
    border: {
        marginLeft: 5,
        borderBottomColor: '#003366',
        borderBottomWidth: 1,
        width: 400
    },
    git: {
        marginTop: 10,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        marginRight: 140
    },
    accgit: {
        flexDirection: 'row'
    },
    akungitlab: {
       marginLeft: 105,
       fontWeight: 'bold',
       color: '#003366' 
    },
    akungithub: {
        marginLeft: 100,
        fontWeight: 'bold',
        color: '#003366' 
     },
     col1: {
         marginTop: 20,
         marginLeft: 150
     },
     col2: {
         marginTop: 35,
         marginRight: 118
     },
     fb: {
         fontWeight: 'bold',
         fontSize: 15,
         color: '#003366'
     },
     ig: {
        marginTop: 35,
        fontWeight: 'bold',
        fontSize: 15,
        color: '#003366'
    },
    tw: {
        marginTop: 35,
        fontWeight: 'bold',
        fontSize: 15,
        color: '#003366'
    }
})