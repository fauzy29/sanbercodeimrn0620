import React from 'react';
import { StyleSheet, 
    Text, 
    View, 
    TouchableOpacity, } from 'react-native';

export default function FlatButton({text}) {
    return (
        <TouchableOpacity> 
            <View style={styles.button}>
                <Text style={styles.buttonText}>{ text }</Text>
            </View>
        </TouchableOpacity>
    )
}

export function FlatButton2({text}) {
    return (
        <TouchableOpacity> 
            <View style={styles.button2}>
                <Text style={styles.buttonText}>{ text }</Text>
            </View>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    button: {
        borderRadius: 24,
        paddingVertical: 14,
        paddingHorizontal: 10,
        backgroundColor: '#3EC6FF',
        width: 150,
        marginLeft: 145,
        marginTop: 5
    },
    button2: {
        borderRadius: 24,
        paddingVertical: 14,
        paddingHorizontal: 10,
        backgroundColor: '#003366',
        width: 150,
        marginLeft: 145,
        marginTop: 5
    },
    buttonText: {
        color: 'white',
        fontWeight: 'bold',
        textAlign: 'center'
    }
})