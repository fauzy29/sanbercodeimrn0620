import React from 'react';
import { StyleSheet, 
    Text, 
    View, 
    Image, 
    TouchableOpacity, 
    TextInput, 
    TouchableWithoutFeedback, 
    Keyboard,
    Button } from 'react-native';
import FlatButton from './Button';
import FlatButton2 from './Button2';

// menghilangkan keyboard dari layar manapun
const DismissKeyboard = ({ children }) => (
    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
        {children}
    </TouchableWithoutFeedback>
);

export default class LoginScreen extends React.Component {
    render() {
        return(
            <DismissKeyboard>
            <View style={styles.container}>
                <View style={styles.logo}>
                    <Image style={styles.image} source={require('./image/logo-sanber.png')} />
                </View> 
                <View style={styles.title}>
                    <Text style={styles.titleText}>Login</Text>
                </View>
                <View style={styles.inputContainer}>
                <Text style={styles.label}>Username / Email</Text>
                    <View style={styles.inputText}>
                <TextInput style={styles.input} />
                    </View>
                </View>
                <View style={styles.inputContainer}> 
                <Text style={styles.label}>Password</Text>
                     <View style={styles.inputText}>
                <TextInput style={styles.input} secureTextEntry={true}/>
                </View>
                </View> 
                
                <FlatButton text='Masuk'/>
                <Text style={styles.atau}>atau</Text>
                <FlatButton2 text='Daftar ?'/>
            </View>
            </DismissKeyboard>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    image: {
       
    },
    logo: {
        marginTop: 50,
        alignItems: 'center',
        justifyContent: 'center'
    },
    title: {
        alignItems: 'center',
        justifyContent: 'center',
        padding: 70,
        fontWeight: 'bold',
        marginBottom: -20
    },
    titleText: {
        fontSize: 32,
        color: '#003366'
    },
    inputText: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    input: {
        borderWidth: 1,
        borderColor: '#003366',
        width: 300,
        padding: 10
    },
    label: {
        marginLeft: 60,
        color: '#003366',
        fontWeight: 'bold',
        fontSize: 15,
        marginBottom: 2
    },
    inputContainer: {
        marginBottom: 20
    },
    atau: {
       marginLeft: 205,
       fontSize: 16,
       fontWeight: 'bold',
       color: '#3EC6FF',
       paddingVertical: 10 
    }
})