import React from 'react';
import { StyleSheet, 
    Text, 
    View, 
    Image, 
    TouchableOpacity, 
    TextInput, 
    TouchableWithoutFeedback, 
    Keyboard,
    Button } from 'react-native';
import FlatButton from './Component/Button';
import FlatButton2 from './Component/Button2';

// menghilangkan keyboard dari layar manapun
const DismissKeyboard = ({ children }) => (
    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
        {children}
    </TouchableWithoutFeedback>
);

export default function LoginScreen({ navigation }) {
        return(
            <DismissKeyboard>
            <View style={styles.container}>
                <View style={styles.logo}>
                    <Image style={styles.image} source={require('./image/logo-sanber.png')} />
                </View> 
                <View style={styles.title}>
                    <Text style={styles.titleText}>Login</Text>
                </View>
                <View style={styles.inputContainer}>
                <Text style={styles.label}>Username / Email</Text>
                    <View style={styles.inputText}>
                <TextInput style={styles.input} />
                    </View>
                </View>
                <View style={styles.inputContainer}> 
                <Text style={styles.label}>Password</Text>
                     <View style={styles.inputText}>
                <TextInput style={styles.input} secureTextEntry={true}/>
                </View>
                </View> 
                
                <TouchableOpacity onPress={() => navigation.navigate('Home')}> 
                    <View style={styles.button2}>
                        <Text style={styles.buttonText}>Login</Text>
                    </View>
                </TouchableOpacity>
                <Text style={styles.atau}>atau</Text>
                <TouchableOpacity onPress={() => navigation.navigate('Register')}> 
                    <View style={styles.button}>
                        <Text style={styles.buttonText}>Daftar</Text>
                    </View>
                </TouchableOpacity>
            </View>
            </DismissKeyboard>
        )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: -50
    },
    image: {
       width: 450,
       height: 130,
    },
    logo: {
        marginTop: 50,
        alignItems: 'center',
        justifyContent: 'center'
    },
    title: {
        alignItems: 'center',
        justifyContent: 'center',
        padding: 70,
        fontWeight: 'bold',
        marginBottom: -20
    },
    titleText: {
        fontSize: 32,
        color: '#003366'
    },
    inputText: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    input: {
        borderWidth: 1,
        borderColor: '#003366',
        width: 300,
        padding: 10
    },
    label: {
        marginLeft: 60,
        color: '#003366',
        fontWeight: 'bold',
        fontSize: 15,
        marginBottom: 2
    },
    inputContainer: {
        marginBottom: 20
    },
    atau: {
       marginLeft: 205,
       fontSize: 16,
       fontWeight: 'bold',
       color: '#3EC6FF',
       paddingVertical: 10 
    },
    button2: {
        borderRadius: 24,
        paddingVertical: 14,
        paddingHorizontal: 10,
        backgroundColor: '#3EC6FF',
        width: 150,
        marginLeft: 145,
        marginTop: 5
    },
    button: {
        borderRadius: 24,
        paddingVertical: 14,
        paddingHorizontal: 10,
        backgroundColor: '#003366',
        width: 150,
        marginLeft: 145,
        marginTop: 5
    },
    buttonText: {
        color: 'white',
        fontWeight: 'bold',
        textAlign: 'center'
    }
})