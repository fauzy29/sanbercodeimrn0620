import React from "react";
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { AuthContext } from "./context";

import LoginScreen from './LoginScreen';
import RegisterScreen from './RegisterScreen';
import SkillScreen from './SkillScreen';
import ProjectScreen from './ProjectScreen';
import AddScreen from './AddScreen';
import AboutScreen from './AboutScreen';

import { StyleSheet, Text, View } from "react-native";

// const Details1 = () => (
//   <View>
//     <Text>Details Screen</Text>
//   </View>
// );

// const Details2 = () => (
//   <View>
//     <Text>Details Screen</Text>
//   </View>
// );

// const Details3 = () => (
//   <View>
//     <Text>Details Screen</Text>
//   </View>
// );



const Tabs = createBottomTabNavigator();
// menu bottom dan content nya
const TabsScreen = () => {
  return (
    <Tabs.Navigator>
    <Tabs.Screen name='Skill' component={SkillScreen}/>
    <Tabs.Screen name='Project' component={ProjectScreen}/>
    <Tabs.Screen name='Add' component={AddScreen} />
  </Tabs.Navigator>
  )
}

const Drawer = createDrawerNavigator();
const DrawerScreen = () => {
  return (
    <Drawer.Navigator>
      <Drawer.Screen name='Home' component={TabsScreen} />
      <Drawer.Screen name='About' component={AboutScreen} />
    </Drawer.Navigator>
  )
}

const RootStack = createStackNavigator();
const RootStackScreen = () => {
  return (
    <RootStack.Navigator>
      <RootStack.Screen name="Login" component={LoginScreen}/>
      <RootStack.Screen name="Register" component={RegisterScreen}/>
      <RootStack.Screen name="Home" component={DrawerScreen}/>
    </RootStack.Navigator>
  )
}

export default () => (
   <NavigationContainer>
       <RootStackScreen />
   </NavigationContainer>
)