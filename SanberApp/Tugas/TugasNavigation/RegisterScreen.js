import React from 'react';
import { StyleSheet, 
    Text, 
    View, 
    Image, 
    TouchableOpacity, 
    TextInput, 
    TouchableWithoutFeedback, 
    Keyboard,
    Button,
    KeyboardAvoidingView } from 'react-native';
import FlatButton from './Component/Button';
import FlatButton2 from './Component/Button2';

// menghilangkan keyboard dari layar manapun
const DismissKeyboard = ({ children }) => (
    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
        {children}
    </TouchableWithoutFeedback>
);

export default function RegisterScreen({navigation}) {
        return(
            <KeyboardAvoidingView
            behavior={Platform.OS == "ios" ? "padding" : "height"}
            style={styles.container}
            >
            <DismissKeyboard>
            <View style={styles.container}>
                <View style={styles.logo}>
                    <Image style={styles.image} source={require('./image/logo-sanber.png')} />
                </View> 
                <View style={styles.title}>
                    <Text style={styles.titleText}>Register</Text>
                </View>
                <View style={styles.inputContainer}>
                <Text style={styles.label}>Username</Text>
                    <View style={styles.inputText}>
                <TextInput style={styles.input} />
                    </View>
                </View>
                <View style={styles.inputContainer}>
                <Text style={styles.label}>Email</Text>
                    <View style={styles.inputText}>
                <TextInput style={styles.input} />
                    </View>
                </View>
                <View style={styles.inputContainer}> 
                <Text style={styles.label}>Password</Text>
                     <View style={styles.inputText}>
                <TextInput style={styles.input} secureTextEntry={true}/>
                </View>
                </View> 
                <View style={styles.inputContainer}> 
                <Text style={styles.label}>Ulangi Password</Text>
                     <View style={styles.inputText}>
                <TextInput style={styles.input} secureTextEntry={true}/>
                </View>
                </View> 
                
                <TouchableOpacity onPress={() => navigation.navigate('Home')}> 
                    <View style={styles.button}>
                        <Text style={styles.buttonText}>Daftar</Text>
                    </View>
                </TouchableOpacity>
                <Text style={styles.atau}>atau</Text>
                <TouchableOpacity onPress={() => navigation.navigate('Login')}> 
                    <View style={styles.button2}>
                        <Text style={styles.buttonText}>Login</Text>
                    </View>
                </TouchableOpacity>
            </View>
            </DismissKeyboard>
            </KeyboardAvoidingView>
        )
    }

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: -35
    },
    logo: {
        marginTop: 50,
        alignItems: 'center',
        justifyContent: 'center'
    },
    title: {
        alignItems: 'center',
        justifyContent: 'flex-end',
        padding: 70,
        fontWeight: 'bold',
        marginBottom: -60
    },
    titleText: {
        fontSize: 32,
        color: '#003366'
    },
    inputText: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    input: {
        borderWidth: 1,
        borderColor: '#003366',
        width: 300,
        padding: 10
    },
    label: {
        marginLeft: 60,
        color: '#003366',
        fontWeight: 'bold',
        fontSize: 15,
        marginBottom: 2
    },
    inputContainer: {
        marginBottom: 20
    },
    atau: {
       marginLeft: 205,
       fontSize: 16,
       fontWeight: 'bold',
       color: '#3EC6FF',
       paddingVertical: 10 
    },
    image: {
        width: 450,
        height: 130,
     },
     button2: {
        borderRadius: 24,
        paddingVertical: 14,
        paddingHorizontal: 10,
        backgroundColor: '#3EC6FF',
        width: 150,
        marginLeft: 145,
        marginTop: 5
    },
    button: {
        borderRadius: 24,
        paddingVertical: 14,
        paddingHorizontal: 10,
        backgroundColor: '#003366',
        width: 150,
        marginLeft: 145,
        marginTop: 5
    },
    buttonText: {
        color: 'white',
        fontWeight: 'bold',
        textAlign: 'center'
    }
})