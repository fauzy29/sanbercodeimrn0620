import React, { Component } from 'react';
import { Image, Text, View, FlatList, StyleSheet, TouchableOpacity } from 'react-native';
import Data from './skillData.json';
import { isRequired } from 'react-native/Libraries/DeprecatedPropTypes/DeprecatedColorPropType';

import Icon from  'react-native-vector-icons/MaterialIcons';
import data from './skillData.json';
import SkillItem from './components/skillItem';

export default class SkillScreen extends Component {
    render() {
        return(
            <View style={styles.container}>
                <View style={styles.navbar}>
                <View style={styles.left}>
                <TouchableOpacity>
                    <Icon style={styles.iconStyle} name='account-circle' size={30} color='#3EC6FF'/>
                </TouchableOpacity>
                <View style={styles.greetText}>
                    <Text style={styles.greeting}>Hai,</Text>
                    <Text style={{color: '#003366', fontWeight: 'bold'}}>Tubagus Gusti Fauzy</Text>
                </View>
                </View>
                    <View style={styles.rightLogo}>
                        <Image style={styles.image} source={require('../Tugas13/image/logo-sanber.png')} />
                    </View>
                </View>

                <View style={styles.body}>
                    <Text style={styles.title}>SKILL</Text>
                    <View style={{borderBottomColor: '#3EC6FF', borderBottomWidth: 3}}></View>
                    <View style={styles.catalog}>
                        <TouchableOpacity>
                                <Text style={styles.catalogText}>Library / Framework</Text>
                        </TouchableOpacity>
                        <TouchableOpacity>
                                <Text style={styles.catalogText}>Bahasa Pemrograman</Text>
                        </TouchableOpacity>
                        <TouchableOpacity>
                                <Text style={styles.catalogText}>Teknologi</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.skillList}>
                        <FlatList 
                           data={data.items}
                           renderItem={(skill) => <SkillItem skill={skill.item} />}
                           keyExtractor={(item) => item.id} 
                        />
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    body: {
        backgroundColor: 'white',
        paddingHorizontal: 20,
    },
    navbar: {
        height: 150,
        backgroundColor: 'white',
        paddingHorizontal: 10,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    image: {
        width: 200,
        height: 60,
        marginBottom: 40
    },
    rightLogo: {
        flexDirection: 'row'
    },
    iconStyle: {
        paddingHorizontal: 10,
        marginTop: 50
    },  
    left:{
        flexDirection: 'row',
        marginTop: 45
    },
    greetText:{
        flexDirection: 'column',
    },
    greeting: {
        marginTop: 50,
        color: '#003366',
        fontWeight: 'bold'
    },
    title: {
        fontSize: 42,
        color: '#003366',
    },
    catalogText: {
        fontWeight: 'bold',
        color: '#003366',
        backgroundColor: '#B4E9FF',
        padding: 8,
        borderRadius: 10,
        fontSize: 16
    },
    catalog: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingTop: 10
    },
    skillList: {
        height: 550,
        justifyContent: 'flex-start'
    }
})