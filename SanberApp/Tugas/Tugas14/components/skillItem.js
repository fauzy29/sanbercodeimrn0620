import React, { Component } from 'react';
import { Image, Text, View, FlatList, StyleSheet, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'; 


export default class SkillItem extends Component {
    render() {
        let skill = this.props.skill;
        return (
            <View style={styles.container}>
                <View style={styles.skillBox}>
                    <View style={styles.logoSkill}>
                    <Icon name={skill.iconName} size={100} color='#003366'/>
                    </View>
                    <View style={styles.skillDesc}>
                        <Text style={{fontSize: 32, fontWeight: 'bold', color:'#003366'}}>{skill.skillName}</Text>
                        <Text style={{fontSize: 20, fontWeight: 'bold', color:'#3EC6FF'}}>{skill.categoryName}</Text>
                        <Text style={{fontSize: 80, fontWeight: 'bold', color:'#FFFFFF', marginLeft:20}}>{skill.percentageProgress}</Text>
                    </View>
                    <TouchableOpacity>
                    <View style={styles.arrow}>
                        <Icon style={styles.arrows} name='chevron-right' size={130} color='#003366'/>
                    </View>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
       paddingVertical: 8,
    },
    skillBox: {
        width: 380,
        height: 150,
        backgroundColor: '#B4E9FF',
        borderRadius: 15,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 4,
        },
        shadowOpacity: 0.32,
        shadowRadius: 5.46,
        elevation: 9,
        flexDirection: 'row',
    },
    logoSkill: {
        alignItems: 'center',
        justifyContent: 'center',
    },
    skillDesc: {
        flexDirection: 'column',
        alignItems: 'flex-start',
        justifyContent: 'center',
        marginLeft: 120,
        marginBottom: -15,
        position: 'absolute'
    },
    arrow: {
        flexDirection: 'row'
    },
    arrows: {
        alignItems: 'flex-end',
        justifyContent: 'center',
        marginLeft: 180,
        marginTop: 10
    }
})