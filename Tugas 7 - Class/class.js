class Animal {
    constructor(name) {
        this.name = name;
        this.jumlahKaki = 4;
        this.darahDingin = false;
    }
    get legs() {
        return this.jumlahKaki;
    }
    get cold_blooded() {
        return this.darahDingin;
    }
}

class Ape extends Animal {
    constructor(name) {
        super(name);
        this.jumlahKaki = 2;
    }
    yell() {
        console.log("Auooo");
    }
}

class Frog extends Animal {
    jump() {
        console.log("hop hop");
    }
}

class Clock {
    constructor({template}) {
        this.template = template;
    }

    render() {
        let date = new Date();

        let hours = date.getHours();
        if (hours < 10) hours = '0' + hours;

        let mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;

        let secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;

        let output = this.template
        .replace('h', hours)
        .replace('m', mins)
        .replace('s', secs);

        console.log(output);
    }

    stop() {
        clearInterval(this.timer);
    }

    start() {
        this.render();
        this.timer = setInterval(() => this.render(), 1000);
    }
}

let sheep = new Animal('Shaun');

console.log(sheep.name);
console.log(sheep.legs);
console.log(sheep.cold_blooded);

console.log('\t');

let sungokong = new Ape('Kera Sakti');
sungokong.yell();

console.log('\t');

let kodok = new Frog('Buduk');
kodok.jump();

console.log('\t');
let clock = new Clock({template: 'h:m:s'});
clock.start();