console.log('LOOPING PERTAMA');
console.log('\t');

let pertama = 2;
while (pertama <= 20) {
    console.log(pertama + ' - I love coding');

    pertama += 2;
}

console.log('\t');
console.log('LOOPING KEDUA');
console.log('\t');

let kedua = 20;
while(kedua >= 2) {
    console.log(kedua + ' - I will become a mobile developer');

    kedua -= 2;
}

console.log('\t');
console.log('LOOPING KETIGA');
console.log('\t');

for (let ketiga = 1; ketiga <= 20; ketiga++) {
      if (ketiga % 3 == 0 && ketiga % 2 == 1) {
        console.log(ketiga + ' - I Love Coding');
    } else if (ketiga % 2 == 1) {
        console.log(ketiga + ' - Santai');
    } else if (ketiga % 2 == 0) {
        console.log(ketiga + ' - Berkualitas');
    }
}

console.log('\t');
console.log('LOOPING KEEMPAT');
console.log('\t');

for (let i = 1; i <= 4; i++) {
     for (let j = 1; j <= 8; j++) {
         process.stdout.write('#');
    }
    process.stdout.write('\n');
}

console.log('\t');
console.log('LOOPING KELIMA');
console.log('\t');

for (let k = 1; k <= 7; k++) {
    for (let l = 1; l <= k; l++) {
        process.stdout.write('#');
    }
    process.stdout.write('\n');
}

console.log('\t');
console.log('LOOPING KEENAM');
console.log('\t');

for (let m = 1; m <= 8; m++) {
    for (let n = 1; n <= 8; n++) {
        let total = m + n;
        if (total % 2 == 0) {
            process.stdout.write(' '); 
        } else if (total % 2 == 1) {
            process.stdout.write('#');
        }
   }
   process.stdout.write('\n');
}

