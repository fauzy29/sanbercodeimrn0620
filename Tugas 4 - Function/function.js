function teriak() {
    return "Halo Sanbers!";
}

function kalikan(num1, num2) {
    return num1 * num2;
}

function introduce(name, age, address, hobby) {
    return `Nama saya ${name}, umur saya ${age} tahun, alamat saya di ${address}, dan saya punya hobby yaitu ${hobby}!`;
}

console.log(teriak());

let num1 = 12;
let num2 = 4;
let hasilKali = kalikan(num1, num2);
console.log(hasilKali);

let name = "Fauzy";
let age = 17;
let address = "Jln. Panteneun, Sumedang";
let hobby = "Gaming";

let perkenalan = introduce(name, age, address, hobby);
console.log(perkenalan);