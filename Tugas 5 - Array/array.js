function range(startNum, finishNum) {
    let arr1 = [];
    if (startNum < finishNum) {
        for (let i = startNum; i <= finishNum; i++) {
            arr1.push(i);   
        }
    } else if (startNum > finishNum) {
        for (let i = startNum; i >= finishNum; i--) {
            arr1.push(i);   
        }
    }  else {
        arr1 = -1;
    }
    // if (ndak tau kondisinya apaa) {
    //     arr1 = arr1.sort((a, b) => b - a);
    // } else {
    //     arr1 = arr1.sort((a, b) => a - b);
    // }
    return arr1;
}

function rangeWithStep (startNum, finishNum, step) {
    let arr1 = [];
    if (startNum < finishNum) {
        for (let i = startNum; i <= finishNum; i += step) {
            arr1.push(i);   
        }
    } else if (startNum > finishNum) {
        for (let i = startNum; i >= finishNum; i -= step) {
            arr1.push(i);   
        }
    }  else {
        arr1 = -1;
    }
    return arr1;
}

function sum (startNum = 0, finishNum = 0, step = 1) {
    let arr1 = [];
    let total = 0;
    if (startNum < finishNum) {
        for (let i = startNum; i <= finishNum; i += step) {
            arr1.push(i);   
        }
    } else if (startNum > finishNum) {
        for (let i = startNum; i >= finishNum; i -= step) {
            arr1.push(i);   
        }
    }  else {
        arr1 = -1;
    }

    for (let i in arr1) {
        total = total + arr1[i];
    }
    return total;

}

function dataHandling(array) {
    for (let i = 0; i < array.length; i++) {
        console.log('Nomor ID: ' + array[i][0]);
        console.log('Nama Lengkap: ' + array[i][1]);
        console.log('TTL : ' + array[i][2] + ' ' + array[i][3]);
        console.log('Hobi : ' + array[i][4]);
        console.log('\t');
    }
}

function balikKata(string) {
    let balik = "";
    for (let i = string.length - 1; i >= 0; i--) {
        balik += string[i];
    }
    return balik;
}

function dataHandling2(array) {
    array.splice(1, 1, "Roman Alamsyah Elsharawy");
    array.splice(2, 1, "Provinsi Bandar Lampung");
    array.splice(4, 2, "Pria", "SMA Internasional Metro");
    console.log(array);

    let month = array[3].split('/');
    let slug = month.join('-');
    let irisan = array[1].slice(0, 15);

    let bulan = month[1];
    switch(bulan) {
        case '01' : 
        bulan = 'Januari';
        break;
        case '02' : 
        bulan = 'Februari';
        break;
        case '03' : 
        bulan = 'Maret';
        break;
        case '04' : 
        bulan = 'April';
        break;
        case '05' : 
        bulan = 'Mei';
        break;
        case '06' : 
        bulan = 'Juni';
        break;
        case '07' : 
        bulan = 'Juli';
        break;
        case '08' : 
        bulan = 'Agustus';
        break;
        case '09' : 
        bulan = 'September';
        break;
        case '10' : 
        bulan = 'Oktober';
        break;
        case '11' : 
        bulan = 'November';
        break;
        case '12' : 
        bulan = 'Desember';
        break;
        default :
        bulan = 'Bulan yang anda masukkan salah';
        break;
    }
    console.log(bulan);

    let sortBulan = month.sort((a, b) => b - a);
    console.log(sortBulan);

    console.log(slug);
    console.log(irisan);

    return '';
}

console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 

console.log('\n');

console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 

console.log('\n');

console.log(sum(1,10)); // 55
console.log(sum(5, 50, 2)); // 621
console.log(sum(15,10)); // 75
console.log(sum(20, 10, 2)); // 90
console.log(sum(1)); // 1
console.log(sum()); // 0 

console.log('\n');

var data = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
];

dataHandling(data);

console.log('\n');

console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 

console.log('\n');

let input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
console.log(dataHandling2(input));






