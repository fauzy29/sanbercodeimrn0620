function arrayToObject(array) {
   const dateNow = new Date();
   let thisYear = dateNow.getFullYear();
   let data = {};
   for (let i = 0; i < array.length; i++) {
       data = {
           firstName: array[i][0],
           lastName: array[i][1],
           gender: array[i][2],
           age: thisYear - array[i][3]  
       };
       if (isNaN(array[i][3]) || array[i][3] > thisYear) {
           data.age = "Invalid Birth Year";
       }
       console.log(i + 1 + '.' + ' ' + array[i][0] + ' ' + array[i][1], data);
   }
}

function shoppingTime(memberId, money) {
    let shopping = {};
    let barang = [
        ['Sepatu Staccatu', 1500000],
        ['Baju Zoro', 500000],
        ['Baju H&N', 250000],
        ['Sweater Uniklooh', 175000],
        ['Casing Handphone', 50000]
    ];

    if (memberId == undefined && money == undefined) {
        return 'Mohon maaf, toko X hanya berlaku untuk member saja';
    }

    if (memberId == '') {
        return 'Mohon maaf, toko X hanya berlaku untuk member saja'; 
    } else {
        shopping.memberId = memberId;
    }

    if (money < 50000) {
        return 'Mohon maaf, uang tidak cukup'
    } else {
        shopping.money = money;
    }

    let totalHarga = 0;
    shopping.listPurchased = [];

    for (let i = 0; i < barang.length; i++) {
        if (money > barang[i][1]) {
            shopping.listPurchased.push(barang[i][0]);
            totalHarga += barang[i][1];
        }
        shopping.changeMoney = shopping.money - totalHarga;
    }
    return shopping;
}

function naikAngkot(arrPenumpang) {
    let rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    let penumpang = {};
    for (let i = 0; i < arrPenumpang.length; i++) {
        penumpang = {
            penumpang: arrPenumpang[i][0],
            naikDari: arrPenumpang[i][1],
            tujuan: arrPenumpang[i][2]
        }
        let awal = arrPenumpang[i];
        let sliceAwal = awal.slice(1);
        let hapusTujuan = sliceAwal.shift();
        let hapusNaik = sliceAwal.pop();

        let naik = rute.indexOf(hapusTujuan);
        let berhenti = rute.indexOf(hapusNaik);
        let total = rute.slice(naik, berhenti);
        penumpang.bayar = 2000 * total.length;
        console.log(penumpang);
    }
    return '';
}


let people = [ 
    ["Bruce", "Banner", "male", 1975], 
    ["Natasha", "Romanoff", "female"] 
];
let people2 = [ 
    ["Tony", "Stark", "male", 1980], 
    ["Pepper", "Pots", "female", 2023] 
];

console.log('Soal No 1. Array to Object');
arrayToObject(people);
arrayToObject(people2);
arrayToObject([]);

console.log('\t');

console.log('Soal No 2. Shopping Time');
console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

console.log('\t');

let penumpang = [ 
    ['Dimitri', 'B', 'F'],
    ['Icha', 'A', 'B']
];
console.log('Soal No 3. Naik Angkot');
console.log(naikAngkot(penumpang));




