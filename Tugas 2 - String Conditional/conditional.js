let nama = 'John';
let peran = '';

if (nama == '' && peran == '') {
    console.log('Nama harus diisi!');
} else if (nama && peran == '') {
    console.log(`Halo ${nama}, Pilih peranmu untuk memulai game!`);
} else if (nama && (peran == 'penyihir' || peran == 'Penyihir')) {
    console.log(`Selamat datang di Dunia Werewolf, ${nama}`);
    console.log(`Halo Penyihir ${nama}, kamu dapat melihat siapa yang menjadi werewolf!`);
} else if (nama && (peran == 'guard' || peran == 'Guard')) {
    console.log(`Selamat datang di Dunia Werewolf, ${nama}`);
    console.log(`Halo Guard ${nama}, kamu akan membantu melindungi temanmu dari serangan werewolf!`);
} else if (nama && (peran == 'werewolf' || peran == 'Werewolf')) {
    console.log(`Selamat datang di Dunia Werewolf, ${nama}`);
    console.log(`Halo Werewolf ${nama}, kamu akan memakan mangsa setiap malam!`);
}

console.log('\n');

let tanggal = 29;
let bulan = 12;
let tahun = 2002;

// if untuk tahun dan tanggal karena saya bingung apabila memakai switch
if (tahun < 1900 || tahun > 2200) {
    tahun = 'Tahun yang anda masukkan salah';
}

if (tanggal < 1 || tanggal > 31) {
    tanggal = 'Tanggal yang anda masukkan salah';
}

// switch untuk bulan
switch(bulan) {
    case 1 : 
    bulan = 'Januari';
    break;
    case 2 : 
    bulan = 'Februari';
    break;
    case 3 : 
    bulan = 'Maret';
    break;
    case 4 : 
    bulan = 'April';
    break;
    case 5 : 
    bulan = 'Mei';
    break;
    case 6 : 
    bulan = 'Juni';
    break;
    case 7 : 
    bulan = 'Juli';
    break;
    case 8 : 
    bulan = 'Agustus';
    break;
    case 9 : 
    bulan = 'September';
    break;
    case 10 : 
    bulan = 'Oktober';
    break;
    case 11 : 
    bulan = 'November';
    break;
    case 12 : 
    bulan = 'Desember';
    break;
    default :
    bulan = 'Bulan yang anda masukkan salah';
    break;
}

console.log(tanggal + ' ' + bulan + ' ' + tahun);