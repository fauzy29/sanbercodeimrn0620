let word = 'Javascript';
let second = 'is';
let third = 'awesome';
let fourth = 'and';
let fifth = 'I';
let sixth = 'love';
let seventh = 'it!';

console.log(word + " " + second + " " + third + " " + fourth + " " + fifth + " " + sixth + " " + seventh);

console.log('\n');

let sentence = 'I am going to be React Native Developer';

let exampleFirstWord = sentence[0];
let exampleSecondWord = sentence[2] + sentence[3];
let thirdWord = sentence[5] + sentence[6] + sentence[7] + sentence[8] + sentence[9];
let fourthWord = sentence[11] + sentence[12];
let fifthWord = sentence[14] + sentence[15];
let sixthWord = sentence[17] + sentence[18] + sentence[19] + sentence[20] + sentence[21];
let seventhWord = sentence[23] + sentence[24] + sentence[25] + sentence[26] + sentence[27] + sentence[28];
let eighthWord = sentence[30] + sentence[31] + sentence[32] + sentence[33] + sentence[34] + sentence[35] + sentence[36] + sentence[37] + sentence[38];

console.log('First Word : ' + exampleFirstWord);
console.log('Second Word : ' + exampleSecondWord);
console.log('Third Word : ' + thirdWord);
console.log('Fourth Word : ' + fourthWord);
console.log('Fifth Word : ' + fifthWord);
console.log('Sixth Word : ' + sixthWord);
console.log('Seventh Word : ' + seventhWord);
console.log('Eighth Word : ' + eighthWord);

console.log('\n');

let sentence2 = 'wow Javascript is so cool';

let exampleFirstWord2 = sentence2.substring(0, 3);
let secondWord = sentence2.substring(4, 14);
let thirdWord2 = sentence2.substring(15, 17);
let fourthWord2 = sentence2.substring(18, 20);
let fifthWord2 = sentence2.substring(21, 25);

console.log('First Word : ' + exampleFirstWord2);
console.log('Second Word : ' + secondWord);
console.log('Third Word : ' + thirdWord2);
console.log('Fourth Word : ' + fourthWord2);
console.log('Fifth Word : ' + fifthWord2);

console.log('\n');

let sentence3 = 'wow Javascript is so cool';

let exampleFirstWord3 = sentence3.substring(0, 3);
let secondWord2 = sentence3.substring(4, 14);
let thirdWord3 = sentence3.substring(15, 17);
let fourthWord3 = sentence3.substring(18, 20);
let fifthWord3 = sentence3.substring(21, 25);

let firstWordLength = exampleFirstWord3.length;
let secondWordLength = secondWord2.length;
let thirdWordLength = thirdWord3.length;
let fourthWord3Length = fourthWord3.length;
let fifthWord3Length = fifthWord3.length;

console.log('First Word : ' + exampleFirstWord3 + ', with length = ' + firstWordLength);
console.log('Second Word : ' + secondWord2 + ', with length = ' + secondWordLength);
console.log('Third Word : ' + thirdWord3 + ', with length = ' + thirdWordLength);
console.log('Fourth Word : ' + fourthWord3 + ', with length = ' + fourthWord3Length);
console.log('Fifth Word : ' + fifthWord3 + ', with length = ' + fifthWord3Length);





